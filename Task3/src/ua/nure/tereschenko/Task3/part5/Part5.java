package ua.nure.tereschenko.Task3.part5;

import java.util.Locale;
import java.util.ResourceBundle;
import java.util.Scanner;

public class Part5 {

	private static final String BASE_NAME = "resources";

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in, "Cp1251");
		String line = null;
		while (scanner.hasNextLine()) {
			line = scanner.nextLine();
			if (line.equals("stop")) {
				return;
			}
			Locale locale = new Locale(line.split(" ")[1]);
			ResourceBundle rb = ResourceBundle.getBundle(BASE_NAME, locale);
			System.out.println(rb.getString(line.split(" ")[0]));
		}
	}
}