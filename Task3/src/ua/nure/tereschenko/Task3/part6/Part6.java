package ua.nure.tereschenko.Task3.part6;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Part6 {

	private static final String MAILS = "part6_mails.txt";

	private static final String GROUPS = "part6_groups.txt";

	private static final String USERS = "part6_users.txt";

	private static final String EOL = System.lineSeparator();

	private static final String ENCODING = "Cp1251";

	private static final String REG_OF_LOGIN = "[\\S]+(?=\\;)";

	private static final String REG_OF_EMAIL = "(?<=\\;)[\\S]+";

	public static void filter(Reader mailsReader, Reader groupsReader,
			Writer usersWriter) throws IOException {

		StringBuilder mails = new StringBuilder();
		StringBuilder groups = new StringBuilder();
		BufferedReader mail = new BufferedReader(mailsReader);
		BufferedReader group = new BufferedReader(groupsReader);
		String line = null;
		while ((line = mail.readLine()) != null) {
			mails.append(line).append(EOL);
		}
		line = null;
		while ((line = group.readLine()) != null) {
			groups.append(line).append(EOL);
		}
		mail.close();
		group.close();
		Pattern patternOfLogin = Pattern.compile(REG_OF_LOGIN);
		Matcher matchOfLogin = patternOfLogin.matcher(mails.substring(0,
				mails.length() - EOL.length()));
		Pattern patternOfEmail = Pattern.compile(REG_OF_EMAIL);
		Matcher matchOfEmail = patternOfEmail.matcher(mails.substring(0,
				mails.length() - EOL.length()));
		Matcher matchOfLoginOfGroup = patternOfLogin.matcher(groups.substring(
				0, groups.length() - EOL.length()));
		Matcher matchOfGroups = patternOfEmail.matcher(groups.substring(0,
				groups.length() - EOL.length()));
		Map<String, String> map1 = new HashMap<>();
		Map<String, String> map2 = new HashMap<>();
		String s1 = null;
		String s2 = null;
		while (matchOfLogin.find() && matchOfEmail.find()) {
			s1 = matchOfLogin.group();
			s2 = matchOfEmail.group();
			if (!s1.contains("#")) {
				map1.put(s1, s2);
			}
		}
		s1 = null;
		s2 = null;
		while (matchOfLoginOfGroup.find() && matchOfGroups.find()) {
			s1 = matchOfLoginOfGroup.group();
			s2 = matchOfGroups.group();
			if (!s1.contains("#")) {
				map2.put(s1, s2);
			}
		}
		Set<String> keys1 = map1.keySet();
		Set<String> keys2 = map2.keySet();
		keys1.retainAll(keys2);
		for (String item : keys1) {
			usersWriter.write(item + ";" + map1.get(item) + ";"
					+ map2.get(item) + "\r\n");
		}
		usersWriter.close();
	}

	public static void main(String[] args) throws IOException {

		filter(new BufferedReader(new InputStreamReader(new FileInputStream(
				MAILS), ENCODING)),
				new BufferedReader(new InputStreamReader(new FileInputStream(
						GROUPS), ENCODING)),
				new OutputStreamWriter(new FileOutputStream(USERS), Charset
						.forName("Cp1251")));
	}
}