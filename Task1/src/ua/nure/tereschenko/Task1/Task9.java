package ua.nure.tereschenko.Task1;

public class Task9 {

	public static void usage() {
		System.out.println("Usage: java "
				+ "ua.kharkov.tereschenko.Task1.Task9");
	}

	public static int[][][][][][] fillArray() {
		int[][][][][][] array = new int[2][2][2][2][2][2];
		for (int i = 0, j = 1; i < Math.pow(2, 6); i++, j++) {
			array[(i >> 5) & 1][(i >> 4) & 1]
					[(i >> 3) & 1][(i >> 2) & 1]
					[(i >> 1) & 1][(i >> 0) & 1] = ++i;
			System.out.print("[" + i + ", ");
			array[(i >> 5) & 1][(i >> 4) & 1]
					[(i >> 3) & 1][(i >> 2) & 1]
					[(i >> 1) & 1][(i >> 0) & 1] = ++j;
			System.out.println(j + "]");
		}
		return array;
	}

	public static void main(String[] args) {
		if (args.length > 0) {
			usage();
			return;
		}
		System.out.println("Values of array: ");
		fillArray();
	}

}
