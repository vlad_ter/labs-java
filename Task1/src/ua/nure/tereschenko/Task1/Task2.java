package ua.nure.tereschenko.Task1;

public class Task2 {

	public static void usage() {
		System.out.println("Usage: java "
				+ "ua.kharkov.tereschenko.Task1.Task2 X");
	}

	public static int getSum(int data) {
		int result = 0;
		for (int i = data; i != 0; i /= 10) {
			result += i % 10;
		}
		return (result > 0) ? result : (-result);
	}

	public static void main(String[] args) {
		if (args.length != 1) {
			usage();
			return;
		}
		int data = Integer.parseInt(args[0]);
		int result = getSum(data);

		String output = String.format("Sum of digits in number = %d --> %d",
				data, result);
		System.out.println(output);
	}
}
