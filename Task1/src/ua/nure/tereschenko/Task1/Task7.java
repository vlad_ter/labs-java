package ua.nure.tereschenko.Task1;

import java.util.Arrays;

public class Task7 {

	public static void usage() {
		System.out.println("Usage: java "
				+ "ua.kharkov.tereschenko.Task1.Task7 X");
	}

	public static int[] getArrayOfPrimeNumbers(int n) {
		if (n < 1) {
			throw new ArrayIndexOutOfBoundsException(
					"Value most be more than 0");
		}
		int[] result = new int[n];
		for (int i = 2, count = 0; count < n; i++) {
			if (Task3.isPrime(i)) {
				result[count] = i;
				count++;
			}
		}
		return result;
	}

	public static void main(String[] args) {
		if (args.length != 1) {
			usage();
			return;
		}
		int size = Integer.parseInt(args[0]);
		String output = String.format("Array of prime numbers (size is %d)",
				size);
		System.out.println(output);
		int[] array = getArrayOfPrimeNumbers(size);
		System.out.println(Arrays.toString(array));
	}

}
