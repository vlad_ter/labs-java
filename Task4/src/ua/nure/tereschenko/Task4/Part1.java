package ua.nure.tereschenko.Task4;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.Charset;
import java.util.Random;

public class Part1 {

	public static void process() throws IOException {
		Writer studentsWriter = new OutputStreamWriter(new FileOutputStream(
				Constants.INPUT_RAW), Charset.forName("Cp1251"));
		Random rand = new Random();
		String data;
		for (int i = 0; i < Constants.T + 1; i++) {
			for (int j = 0; j < Constants.T; j++) {
				if (i == 0) {
					studentsWriter.write(Constants.SEP
							+ String.valueOf(rand.nextInt(9) + 7));

				} else if (i == 1) {
					studentsWriter.write(Constants.SEP
							+ String.valueOf(Constants.TEST + (j + 1)));

				} else {
					data = Constants.LASTNAME + String.valueOf(i - 1);
					String info = "";
					int temp = rand.nextInt(100) + 1;
					String mark = (temp % Constants.DIVIDER == 0) ? " "
							: String.valueOf(temp);
					info += Constants.SEP + mark;
					if (j == 0) {
						studentsWriter.write(data);
					}
					studentsWriter.write(info);
				}
			}
			studentsWriter.write("\r\n");
		}
		studentsWriter.close();
	}

	public static void main(String[] args) throws IOException {
		process();
		System.out.println("Success! Created the file with table.");
	}

}
