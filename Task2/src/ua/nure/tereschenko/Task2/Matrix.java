package ua.nure.tereschenko.Task2;

import java.util.Arrays;

public class Matrix {

	private double[][] mass;
	private int rows;
	private int columns;

	public Matrix(double[][] table) {
		this.mass = table.clone();
		this.rows = mass.length;
		this.columns = mass[0].length;
	}

	public Matrix(int c, int r) {
		this.rows = c;
		this.columns = r;
		this.mass = new double[rows][columns];
	}

	public double[][] getMass() {
		return mass.clone();
	}

	public int getRows() {
		return rows;
	}

	public int getColumns() {
		return columns;
	}

	public void add(Matrix matrix) {
		if (rows != matrix.getRows() || columns != matrix.getColumns()) {
			throw new UnsupportedOperationException(
					"Columns and rows must be equals");
		}
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < columns; j++) {
				mass[i][j] += matrix.getMass()[i][j];
			}
		}
	}

	public void mul(double number) {
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < columns; j++) {
				mass[i][j] *= number;
			}
		}
	}

	public void mul(Matrix matrix) {
		if (columns != matrix.getRows()) {
			throw new UnsupportedOperationException(
					"Columns of first matrix must be equals rows of second matrix");
		}
		double[][] array = new double[rows][matrix.getColumns()];
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < matrix.getColumns(); j++) {
				for (int n = 0; n < columns; n++) {
					array[i][j] += mass[i][n] * matrix.getMass()[n][j];
				}
			}
		}
		mass = array;
		rows = mass.length;
		columns = mass[0].length;
	}

	public void transpose() {
		double[][] array = new double[columns][rows];
		for (int i = 0; i < columns; i++) {
			for (int j = 0; j < rows; j++) {
				array[i][j] = mass[j][i];
			}
		}
		mass = array;
		rows = mass.length;
		columns = mass[0].length;
	}

	public void print() {
		for (int i = 0; i < rows; i++) {
			System.out.println(Arrays.toString(mass[i]));
		}
	}

	public static void main(String[] args) {
		Matrix matrix = new Matrix(3, 4);
		System.out.println("Initial action");
		matrix.print();
		double[][] a = new double[][] { { 1.0, 2.0, 3.0, 4.0 },
				{ 5.0, 6.0, 7.0, 8.0 }, { 9.0, 10.0, 11.0, 12.0 } };
		Matrix test = new Matrix(a);
		matrix.add(test);
		System.out.println("Add a matrix");
		matrix.print();
		matrix.mul(2.0);
		System.out.println("Mul(double)");
		matrix.print();
		double[][] b = new double[][] { { 1.0, 2.0 }, { 3.0, 4.0 },
				{ 5.0, 6.0 }, { 7.0, 8.0 } };
		Matrix temp = new Matrix(b);
		matrix.mul(temp);
		System.out.println("Mul(Matrix)");
		matrix.print();
		matrix.transpose();
		System.out.println("After transpose");
		matrix.print();
	}

}
