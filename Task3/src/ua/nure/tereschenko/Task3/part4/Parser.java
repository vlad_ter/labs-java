package ua.nure.tereschenko.Task3.part4;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class Parser implements Iterable<String> {

	private static final String REGEX = "[A-Z]{1}[\\w\\d\\s\\-\\,\\;\\:]+[\\.]{1}";

	private static final String EOL = System.lineSeparator();

	private List<String> result;

	public Parser(String fileName, String encoding)
			throws FileNotFoundException {
		result = new ArrayList<String>();
		String content = null;
		StringBuilder sb = new StringBuilder();
		Scanner s = new Scanner(new File(fileName), encoding);
		while (s.hasNextLine()) {
			sb.append(s.nextLine()).append(EOL);
		}
		content = sb.substring(0, sb.length() - EOL.length());
		s.close();
		Pattern pattern = Pattern.compile(REGEX);
		Matcher matcher = pattern.matcher(content);
		while (matcher.find()) {
			result.add(matcher.group());
		}
	}

	@Override
	public Iterator<String> iterator() {
		return result.iterator();
	}
}