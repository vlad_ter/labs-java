package ua.nure.tereschenko.Task1;

public class Task4 {

	public static void usage() {
		System.out.println("Usage: java "
				+ "ua.kharkov.tereschenko.Task1.Task4 X");
	}

	public static int getFact(int number) {
		if (number == 0) {
			return 1;
		}
		return number * getFact(number - 1);
	}

	public static int getSumOfSeries(int number) {
		if (number < 1) {
			throw new IllegalArgumentException("Number must be positive");
		}
		int sum = 0;
		int count = 1;
		while (count <= number) {
			if ((count & 1) == 1) {
				sum += getFact(count);
			} else {
				sum -= getFact(count);
			}
			count++;
		}
		return sum;
	}

	public static void main(String[] args) {
		if (args.length != 1) {
			usage();
			return;
		}

		int number = Integer.parseInt(args[0]);
		int result = getSumOfSeries(number);

		String output = String.format("Sum of the series number = %d "
				+ "--> %d", number, result);
		System.out.println(output);
	}

}
