package ua.nure.tereschenko.Task4;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class Part4 {

	public static void process(Data data) {
		Map<String, List<String>> info = new HashMap<>();
		for (int i = 1; i <= Constants.T; i++) {
			List<String> students = new ArrayList<>();
			for (int j = 1; j <= Constants.K; j++) {
				if (data.getStudentsMap().get(Constants.LASTNAME + j)
						.get(i - 1) == null) {
					students.add(Constants.LASTNAME + j);
				}
			}
			Collections.sort(students, new Comparator<String>() {

				@Override
				public int compare(String o1, String o2) {
					return o2.compareTo(o1);
				}
			});
			info.put(Constants.TEST + i, students);
		}
		Map<String, List<String>> sortedMap = Utils.sort(info);
		for (Entry<String, List<String>> entry : sortedMap.entrySet()) {
			System.out.println(entry.getKey() + " ==> "
					+ entry.getValue().toString());
		}
	}

	public static void main(String[] args) throws FileNotFoundException {
		Data data = new Data(Constants.INPUT);
		process(data);
	}

}
