package ua.nure.tereschenko.Task1;

public class Task5 {

	public static void usage() {
		System.out.println("Usage: java "
				+ "ua.kharkov.tereschenko.Task1.Task5 X");
	}

	public static int getHappy(int number) {
		if (number > 1 && number % 2 == 0) {
			int result = 0;
			int half = number / 2;
			char[] data1 = new char[half];
			char[] data2 = new char[half];
			data1[0] = '1';
			data2[0] = '9';
			if (half != 1) {
				for (int i = 1; i < data1.length; i++) {
					data1[i] = '0';
					data2[i] = '9';
				}
			}
			int maxvalue = Integer.parseInt(String.valueOf(data2));
			for (int first = Integer.parseInt(String.valueOf(data1)); first <= maxvalue; first++) {
				for (int second = 0; second <= maxvalue; second++) {
					if (Task2.getSum(first) == Task2.getSum(second)) {
						result++;
					}
				}
			}
			return result;
		} else {
			throw new IllegalArgumentException("Illegal values of method");
		}
	}

	public static void main(String[] args) {
		if (args.length != 1) {
			usage();
			return;
		}
		int amount = Integer.parseInt(args[0]);
		String output = String.format("Amount of lucky numbers (%d) --> %d",
				amount, getHappy(amount));
		System.out.println(output);
	}

}
