package ua.nure.tereschenko.Task1;

public class Demo {
	public static void main(String[] args) {
		Task1.main(new String[] { "28", "49" });
		Task2.main(new String[] { "23456" });
		Task3.main(new String[] { "17" });
		Task4.main(new String[] { "5" });
		Task5.main(new String[] { "6" });
		Task6.main(new String[] { "7" });
		Task7.main(new String[] { "10" });
		Task8.main(new String[] { "8", "8" });
		Task9.main(new String[] {});
		Task10.main(new String[] { "7" });
	}
}
