package ua.nure.tereschenko.Task4;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Data {

	public static final String REGEX_OF_VALUES = ";(\\d{1,3}|\\s);(\\d{1,3}|\\s);(\\d{1,3}|\\s);"
			+ "(\\d{1,3}|\\s);(\\d{1,3}|\\s);(\\d{1,3}|\\s)";

	private Map<String, List<Integer>> studentsMap = new HashMap<>();

	private Map<Integer, Integer> numberOfQuestionsMap = new HashMap<>();

	private String firstLine;

	private String fileName;

	public Data(String fileName) throws FileNotFoundException {
		this.fileName = fileName;
		Scanner scanner = new Scanner(new File(fileName), "Cp1251");
		String content = "";
		StringBuilder data = new StringBuilder();
		while (scanner.hasNextLine()) {
			data.append(scanner.nextLine()).append(Constants.EOL);
		}
		content = data.substring(0, data.length() - Constants.EOL.length());
		scanner.close();
		Pattern pattern = Pattern.compile(REGEX_OF_VALUES);
		Matcher matcher = pattern.matcher(content);
		int count = 0;
		String temp;
		while (matcher.find()) {
			if (count == 0) {
				for (int i = 1; i < Constants.K + 1; i++) {
					temp = matcher.group();
					this.firstLine = temp;
					getNumberOfQuestionsMap().put(
							i,
							Integer.parseInt(temp.substring(1).split(
									Constants.SEP)[i - 1]));
				}
			} else {
				List<Integer> marks = new ArrayList<Integer>();
				for (String item : matcher.group().substring(1)
						.split(Constants.SEP)) {
					if (item.equals(" ")) {
						marks.add(null);
					} else {
						marks.add(Integer.parseInt(item));
					}
				}
				getStudentsMap().put(
						Constants.LASTNAME + String.valueOf(count), marks);
			}
			count++;
		}
	}

	public String getFirstLine() {
		return firstLine;
	}

	public String getFileName() {
		return fileName;
	}

	public Map<Integer, Integer> getNumberOfQuestionsMap() {
		return numberOfQuestionsMap;
	}

	public void setNumberOfQuestionsMap(
			Map<Integer, Integer> numberOfQuestionsMap) {
		this.numberOfQuestionsMap = numberOfQuestionsMap;
	}

	public Map<String, List<Integer>> getStudentsMap() {
		return studentsMap;
	}

	public void setStudentsMap(Map<String, List<Integer>> studentsMap) {
		this.studentsMap = studentsMap;
	}
}
