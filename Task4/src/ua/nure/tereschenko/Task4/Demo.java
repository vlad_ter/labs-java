package ua.nure.tereschenko.Task4;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Demo {

	private static void printTitle(int n) {
		System.out.printf("~~~~~~~~~~~~~~~Part%d~~~~~~~~~~~~~~~%n", n);
	}

	public static void main(String[] args) throws IOException {
		printTitle(1);
		Part1.main(new String[0]);

		Data data = new Data(Constants.INPUT_RAW);

		printTitle(2);
		Part2.process(data);

		printTitle(3);
		Part3.process(data);

		printTitle(4);
		Part4.process(data);

		printTitle(5);
		Part5.process(data);

		printTitle(6);
		Part6.process(data);
	}

	// other way with a reflection mechanism
	public static void main2(String[] args) throws NoSuchMethodException,
			SecurityException, ClassNotFoundException, IllegalAccessException,
			IllegalArgumentException, InvocationTargetException, IOException {
		printTitle(1);
		Part1.main(new String[0]);

		Data data = new Data(Constants.INPUT_RAW);

		String currentPack = Demo.class.getPackage().getName();
		for (int n = 2; n <= 6; n++) {
			printTitle(n);

			String name = currentPack + ".Part" + n;
			Method m = Class.forName(name).getMethod("process", Data.class);
			m.invoke(null, data);
		}

	}

}
