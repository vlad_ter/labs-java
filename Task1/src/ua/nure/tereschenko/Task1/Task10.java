package ua.nure.tereschenko.Task1;

import java.util.Arrays;

public class Task10 {

	public static void usage() {
		System.out.println("Usage: java "
				+ "ua.kharkov.tereschenko.Task1.Task10 X");
	}

	public static int[][] getArrayWithKoeff(int number) {
		if (number < 1) {
			throw new ArrayIndexOutOfBoundsException(
					"Value most be more than 0");
		}
		int[][] result = new int[number][];
		for (int i = 0; i < number; i++) {
			result[i] = new int[i + 1];
			result[i][0] = 1;
			for (int j = 1; j < i; j++) {
				result[i][j] = result[i - 1][j] + result[i - 1][j - 1];
			}
			if (i > 0) {
				result[i][i] = 1;
			}
		}
		return result;
	}

	public static void main(String[] args) {
		if (args.length != 1) {
			usage();
			return;
		}
		int number = Integer.parseInt(args[0]);
		int[][] array = getArrayWithKoeff(number);
		String s = String.format("Pascal`s triangle (%d) :", number);
		System.out.println(s);
		for (int i = 0; i < array.length; i++) {
			System.out.println(Arrays.toString(array[i]));
		}
	}

}
