package ua.nure.tereschenko.Task4;

import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class Part3 {

	public static void process(Data data) {
		Map<String, Integer> results = new HashMap<>();
		int sum = 0;
		int count = 0;
		long temp = 0;
		for (int i = 1; i <= Constants.K; i++) {
			for (Integer item : data.getStudentsMap().get(
					Constants.LASTNAME + i)) {
				if (item != null) {
					sum += item;
					count++;
				}
			}
			temp = Math.round((float) sum / (float) count);
			results.put(Constants.LASTNAME + i, (int) temp);
			sum = 0;
			count = 0;
		}
		Map<String, Integer> sortedMap = Utils.sortByComparator(results);
		for (Entry<String, Integer> entry : sortedMap.entrySet()) {
			System.out.println(entry.getKey() + " ==> " + entry.getValue());
		}
	}

	public static void main(String[] args) throws FileNotFoundException {
		Data data = new Data(Constants.INPUT);
		process(data);
	}

}
