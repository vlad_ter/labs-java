package ua.nure.tereschenko.Task2;

public class Circle {

	private double x, y;

	private double radius;

	public Circle(double x, double y, double r) {
		this.x = x;
		this.y = y;
		this.radius = r;
	}

	public Circle() {
		this(0, 0, 0);
	}

	public void move(double dx, double dy) {
		this.x += dx;
		this.y += dy;
	}

	public void print() {
		System.out.println(this.toString());
	}

	@Override
	public String toString() {
		return "Circle [x = " + this.x + ", y = " + this.y + ", radius = "
				+ this.radius + "]";
	}

	public boolean isInside(double x, double y) {
		double distance = Math.sqrt(Math.pow(this.x - x, 2)
				+ Math.pow(this.y - y, 2));
		return distance < radius;

	}

	public boolean isInside(Circle c) {
		double distance = Math.sqrt(Math.pow(this.x - c.x, 2)
				+ Math.pow(this.y - c.y, 2));
		return distance + c.radius < radius;
	}

	public static void main(String[] args) {
		Circle circle = new Circle(2, 1, 3);
		System.out.println("Initial cirlce");
		circle.print();
		System.out.println("Circle after move");
		circle.move(4, 4);
		circle.print();
		Circle test = new Circle(6, 6, 1);
		System.out.println("Point (1, 1) is inside in " + circle + " --> "
				+ circle.isInside(1, 1));
		System.out.println(test + " is inside in " + circle + " --> "
				+ circle.isInside(test));
	}
}