package ua.nure.tereschenko.Task4;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public final class Utils {

	public static List<Integer> getPossibleMarks(int questionsNumber) {
		List<Integer> results = new ArrayList<Integer>();
		for (double i = 0; i <= questionsNumber; i++) {
			long temp = Math.round((i / questionsNumber) * 100);
			results.add((int) temp);
		}
		return results;
	}

	public static int getNearest(int mark, List<Integer> marks) {
		int module = Math.abs(mark - marks.get(0));
		int number = 0;
		for (int i = 0; i < marks.size(); i++) {
			if (Math.abs(mark - marks.get(i)) < module) {
				module = Math.abs(mark - marks.get(i));
				number = marks.get(i);
			}
		}
		return number;
	}

	public static Map<String, Integer> sortByComparator(
			Map<String, Integer> unsortMap) {
		List<Entry<String, Integer>> list = new LinkedList<Entry<String, Integer>>(
				unsortMap.entrySet());
		Collections.sort(list, new Comparator<Entry<String, Integer>>() {
			public int compare(Entry<String, Integer> o1,
					Entry<String, Integer> o2) {
				if (o1.getValue().equals(o2.getValue())) {
					return o1.getKey().compareTo(o2.getKey());
				}
				return o2.getValue().compareTo(o1.getValue());
			}
		});
		Map<String, Integer> sortedMap = new LinkedHashMap<String, Integer>();
		for (Entry<String, Integer> entry : list) {
			sortedMap.put(entry.getKey(), entry.getValue());
		}
		return sortedMap;
	}
	
	public static Map<String, List<String>> sort(
			Map<String, List<String>> unsortMap) {

		List<Entry<String, List<String>>> list = new LinkedList<Entry<String, List<String>>>(
				unsortMap.entrySet());
		Collections.sort(list, new Comparator<Entry<String, List<String>>>() {

			@Override
			public int compare(Entry<String, List<String>> o1,
					Entry<String, List<String>> o2) {
				if (o1.getValue().size() == o2.getValue().size()) {
					return o1.getKey().compareTo(o2.getKey());
				}
				if (o1.getValue().size() < o2.getValue().size()) {
					return 1;
				} else {
					return -1;
				}
			}

		});
		Map<String, List<String>> sortedMap = new LinkedHashMap<>();
		for (Entry<String, List<String>> entry : list) {
			sortedMap.put(entry.getKey(), entry.getValue());
		}
		return sortedMap;
	}
}
