package ua.nure.tereschenko.Task1;

import java.util.Arrays;

public class Task8 {

	static final char FIRST_LETTER = '�';
	static final char SECOND_LETTER = '�';

	public static void usage() {
		System.out.println("Usage: java "
				+ "ua.kharkov.tereschenko.Task1.Task8 X Y");
	}

	public static char[][] getArray(int n, int m) {
		if (n < 1 || m < 1) {
			throw new IllegalArgumentException("Values must be positive");
		}
		char[][] result = new char[n][m];
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < m; j++) {
				if ((i & 1) == 0) {
					if ((j & 1) == 0) {
						result[i][j] = FIRST_LETTER;
					} else {
						result[i][j] = SECOND_LETTER;
					}
				} else {
					if ((j & 1) == 0) {
						result[i][j] = SECOND_LETTER;
					} else {
						result[i][j] = FIRST_LETTER;
					}
				}
			}
		}
		return result;
	}

	public static void main(String[] args) {
		if (args.length != 2) {
			usage();
			return;
		}
		int n = Integer.parseInt(args[0]);
		int m = Integer.parseInt(args[1]);

		String output = String.format("Chessboard %d*%d", n, m);
		System.out.println(output);
		char[][] array = getArray(n, m);
		for (int i = 0; i < array.length; i++) {
			System.out.println(Arrays.toString(array[i]));
		}
	}

}
