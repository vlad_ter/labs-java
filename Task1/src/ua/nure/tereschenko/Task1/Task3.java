package ua.nure.tereschenko.Task1;

public class Task3 {

	public static void usage() {
		System.out.println("Usage: java "
				+ "ua.kharkov.tereschenko.Task1.Task3 X");
	}

	public static boolean isPrime(int number) {
		if (number < 2) {
			return false;
		}
		for (int i = 2; i <= Math.sqrt(number); i++) {
			if (number % i == 0) {
				return false;
			}
		}
		return true;
	}

	public static void main(String[] args) {
		if (args.length != 1) {
			usage();
			return;
		}

		int number = Integer.parseInt(args[0]);
		boolean result = isPrime(number);

		String output = String.format("Number = %d is prime --> %b", number,
				result);
		System.out.println(output);
	}

}
