package ua.nure.tereschenko.Task4;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.Charset;
import java.util.Scanner;

public class Part2 {

	public static void process(Data data) throws IOException {
		Scanner scanner = new Scanner(new File(data.getFileName()), "Cp1251");
		String content = "";
		StringBuilder sb = new StringBuilder();
		while (scanner.hasNextLine()) {
			sb.append(scanner.nextLine()).append(Constants.EOL);
		}
		content = sb.substring(0, sb.length() - Constants.EOL.length());
		scanner.close();
		System.out.println(content);
		System.out.println("=======================================");
		Writer studentsWriter = new OutputStreamWriter(new FileOutputStream(
				Constants.INPUT), Charset.forName("Cp1251"));
		String temp;
		for (int i = 0; i < Constants.T + 1; i++) {
			if (i == 0) {
				studentsWriter.write(Constants.SEP
						+ data.getFirstLine().substring(1));
				studentsWriter.write("\r\n");
				continue;
			}
			for (int j = 0; j < Constants.T; j++) {
				if (i == 1) {
					studentsWriter.write(Constants.SEP
							+ String.valueOf(Constants.TEST + (j + 1)));
					continue;
				} else {
					temp = Constants.LASTNAME + String.valueOf(i - 1);
					String info = "";
					int numberQuestions = data.getNumberOfQuestionsMap().get(
							i - 1);
					if (data.getStudentsMap().get(temp).get(j) != null) {
						int number = Utils.getNearest(data.getStudentsMap()
								.get(temp).get(j),
								Utils.getPossibleMarks(numberQuestions));
						info += Constants.SEP + String.valueOf(number);
					} else {
						info += Constants.SEP + " ";
					}
					if (j == 0) {
						studentsWriter.write(temp);
					}
					studentsWriter.write(info);
				}
			}
			studentsWriter.write("\r\n");
		}
		studentsWriter.close();
		Scanner sc = new Scanner(new File(Constants.INPUT), "Cp1251");
		String cont = null;
		StringBuilder sbuilder = new StringBuilder();
		while (sc.hasNextLine()) {
			sbuilder.append(sc.nextLine()).append(Constants.EOL);
		}
		cont = sbuilder
				.substring(0, sbuilder.length() - Constants.EOL.length());
		System.out.println(cont);
		sc.close();
	}

	public static void main(String[] args) throws IOException {
		Data data = new Data(Constants.INPUT_RAW);
		process(data);
	}

}
