package ua.nure.tereschenko.Task1;

import java.util.Arrays;

public class Task6 {

	public static void usage() {
		System.out.println("Usage: java "
				+ "ua.kharkov.tereschenko.Task1.Task6 X");
	}

	public static int[] getFib(int n) {
		if (n > 1) {
			int[] result = new int[n];
			result[0] = 1;
			result[1] = 1;
			int i = 2;
			while (i < n) {
				result[i] = result[i - 1] + result[i - 2];
				i++;
			}
			return result;
		} else if (n == 1) {
			return new int[] { 1 };
		} else {
			throw new ArrayIndexOutOfBoundsException(
					"Value most be more than 0");
		}
	}

	public static void main(String[] args) {
		if (args.length != 1) {
			usage();
			return;
		}
		int size = Integer.parseInt(args[0]);

		String output = String.format("Array of Fib (size of array is %d)",
				size);
		System.out.println(output);
		int[] array = getFib(size);
		System.out.println(Arrays.toString(array));
	}

}
