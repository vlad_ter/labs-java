package ua.nure.tereschenko.Task3.part7;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Part7 {

	private static final String FILE_NAME = "part7.csv";

	private static final String OUTPUT = "part7.txt";

	private static final String ENCODING = "Cp1251";

	private static final String REG_OF_NAME = "[�-��������-�_\\d]+\\s[�-��������-�_\\d]+\\s[�-��������-�_\\d]+";

	private static final String REG_OF_DATE = "\\S{2}\\.\\S{2}\\.\\S{4}";

	private static final String REG_OF_TIME = "\\S{2}\\:\\S{2}\\:\\S{2}";

	private static final String SPLITTER = "----------------";

	private static final String EOL = System.lineSeparator();

	private static final int MAX = 10;

	public static void main(String[] args) throws IOException, ParseException {
		String content = null;
		StringBuilder sb = new StringBuilder();
		Scanner s = new Scanner(new File(FILE_NAME), ENCODING);
		int count = 0;
		while (count < MAX) {
			if (count == 0) {
				s.nextLine();
				count++;
				continue;
			} else {
				sb.append(s.nextLine()).append(EOL);
				count++;
			}
		}
		content = sb.substring(0, sb.length() - EOL.length());
		s.close();
		Pattern patternOfName = Pattern.compile(REG_OF_NAME);
		Matcher matchOfName = patternOfName.matcher(content);
		Pattern patternOfDate = Pattern.compile(REG_OF_DATE);
		Matcher matchOfDate = patternOfDate.matcher(content);
		Pattern patternOfTime = Pattern.compile(REG_OF_TIME);
		Matcher matchOfTime = patternOfTime.matcher(content);
		String temp = null;
		count = 0;
		List<String> listOfName = new ArrayList<>();
		List<String> listOfDate = new ArrayList<>();
		List<String> listOfTime = new ArrayList<>();
		while (matchOfName.find()) {
			temp = matchOfName.group();
			if (count % 2 == 0) {
				listOfName.add(temp);
			}
			count++;
		}
		count = 0;
		while (matchOfDate.find()) {
			temp = matchOfDate.group();
			if (count % 3 == 0) {
				listOfDate.add(temp);
			}
			count++;
		}
		count = 0;
		while (matchOfTime.find()) {
			temp = matchOfTime.group();
			if (count % 3 == 0) {
				listOfTime.add(temp);
			}
			count++;
		}
		List<Element> elements = new ArrayList<>();
		for (int i = 0; i < listOfName.size(); i++) {
			elements.add(new Element(listOfName.get(i), listOfDate.get(i) + " "
					+ listOfTime.get(i)));
		}
		Collections.sort(elements, new Comparator<Element>() {

			@Override
			public int compare(Element o1, Element o2) {
				return o1.getDate().compareTo(o2.getDate());
			}
		});
		OutputStreamWriter fw = new OutputStreamWriter(new FileOutputStream(
				OUTPUT), Charset.forName("Cp1251"));
		String result = null;
		for (int i = 0; i < elements.size() - 1; i++) {
			result = String.format("%s ==> %s %s%n", new SimpleDateFormat(
					"yyyy-MM-dd").format(elements.get(i).getDate()),
					new SimpleDateFormat("HH:mm").format(elements.get(i)
							.getDate()), elements.get(i).getName());
			System.out.print(result);
			fw.write(result);
			if (!elements.get(i).getDate()
					.equals(elements.get(i + 1).getDate())) {
				System.out.println(SPLITTER);
				fw.write(SPLITTER + "\r\n");
			}
		}
		String end = String.format("%s ==> %s %s%n", new SimpleDateFormat(
				"yyyy-MM-dd").format(elements.get(elements.size() - 1)
				.getDate()), new SimpleDateFormat("HH:mm").format(elements.get(
				elements.size() - 1).getDate()),
				elements.get(elements.size() - 1).getName());
		System.out.print(end);
		fw.write(end);
		fw.close();
	}
}

class Element {

	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	private Date date;

	public Element(String name, String date) throws ParseException {
		this.name = name;
		SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
		this.date = formatter.parse(date);
	}
}