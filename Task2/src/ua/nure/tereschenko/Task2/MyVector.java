package ua.nure.tereschenko.Task2;

public class MyVector<T> {

	private Object[] array;

	private int size;

	public MyVector(int capacity) {
		array = new Object[capacity];
		size = 0;
	}

	public MyVector() {
		array = new Object[10];
	}

	public int getSize() {
		return size;
	}

	public void add(T element) {
		if (size == array.length) {
			increaseCapacity();
		}
		array[size++] = element;
	}

	public void remove(int j) {
		Object[] temp = new Object[array.length - 1];
		System.arraycopy(array, 0, temp, 0, j);
		System.arraycopy(array, j + 1, temp, j, array.length - j - 1);
		array = temp;
		size = array.length;
	}

	private void increaseCapacity() {
		Object[] temp = new Object[array.length + 1];
		System.arraycopy(array, 0, temp, 0, array.length);
		array = temp;
	}

	public void clear() {
		array = new Object[array.length];
		size = 0;
	}

	@SuppressWarnings("unchecked")
	public T get(int j) {
		if (j >= size) {
			throw new IllegalArgumentException("Cannot obtain element, j = "
					+ j + ", size = " + size);
		}
		return (T) array[j];
	}

	public String toString() {
		if (size == 0) {
			return "Vector []";
		}
		StringBuilder result = new StringBuilder("Vector [");
		for (int j = 0; j < size; j++) {
			result.append(array[j]).append(", ");
		}
		result.delete(result.length() - 2, result.length()).append("]");
		return result.toString();
	}

	public void print() {
		System.out.println(this.toString());
	}

	public static void main(String[] args) {
		System.out.println("Create a new vector");
		MyVector<Integer> a = new MyVector<Integer>(1);
		System.out.printf("Size of vector is %d%n", a.getSize());
		for (int i = 1; i < 7; i++) {
			a.add(i);
			System.out.printf("Added a new element %d%n", i);
		}
		System.out.printf("Size of vector after the change is %d%n",
				a.getSize());
		a.remove(3);
		System.out.printf("Vector after remove: ");
		System.out.println(a);
		System.out.printf("Size of vector after the change is %d%n",
				a.getSize());
		System.out.println("Clear a vector");
		a.clear();
		System.out.printf("Vector after the change: ");
		System.out.println(a);
		System.out.println("Added a new element");
		a.add(18);
		System.out.printf("Last stage of vector: ");
		System.out.println(a);
	}
}
