package ua.nure.tereschenko.Task3;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.text.ParseException;

import ua.nure.tereschenko.Task3.part1.Part1;
import ua.nure.tereschenko.Task3.part2.Part2;
import ua.nure.tereschenko.Task3.part3.Part3;
import ua.nure.tereschenko.Task3.part4.Part4;
import ua.nure.tereschenko.Task3.part5.Part5;
import ua.nure.tereschenko.Task3.part6.Part6;
import ua.nure.tereschenko.Task3.part7.Part7;

public class Demo {

	private static final InputStream STD_IN = System.in;

	public static void main(String[] args) throws IOException, ParseException {
		System.out.println("=========================== PART1");
		Part1.main(args);

		System.out.println("=========================== PART2");
		Part2.main(args);

		System.out.println("\n=========================== PART3");
		System.setIn(new ByteArrayInputStream("int\nstop".getBytes(Charset
				.forName("Cp1251"))));
		Part3.main(args);
		System.setIn(STD_IN);

		System.out.println("\n=========================== PART4");
		Part4.main(args);

		System.out.println("=========================== PART5");
		System.setIn(new ByteArrayInputStream(
				"table ru\ntable en\napple ru\nstop".getBytes(Charset
						.forName("Cp1251"))));
		Part5.main(args);
		System.setIn(STD_IN);

		System.out.println("=========================== PART6");
		Part6.main(args);

		System.out.println("=========================== PART7");

		Part7.main(args);
	}
}