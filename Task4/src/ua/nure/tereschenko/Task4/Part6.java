package ua.nure.tereschenko.Task4;

import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class Part6 {

	public static void process(Data data) {
		Map<String, Integer> results = new HashMap<>();
		int sum = 0;
		int count = 0;
		long temp = 0;
		for (int i = 0; i < Constants.T; i++) {
			for (int j = 1; j <= Constants.K; j++) {
				if (data.getStudentsMap().get(Constants.LASTNAME + j).get(i) != null) {
					sum += data.getStudentsMap().get(Constants.LASTNAME + j)
							.get(i);
					count++;
				}
			}
			temp = Math.round((float) sum / (float) count);
			results.put(Constants.TEST + String.valueOf(i + 1), (int) temp);
			sum = 0;
			count = 0;
		}
		Map<String, Integer> sortedMap = Utils.sortByComparator(results);
		for (Entry<String, Integer> entry : sortedMap.entrySet()) {
			System.out.println(entry.getKey() + " ==> " + entry.getValue());
		}
	}

	public static void main(String[] args) throws FileNotFoundException {
		Data data = new Data(Constants.INPUT);
		process(data);
	}
}
