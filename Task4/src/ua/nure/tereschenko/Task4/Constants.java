package ua.nure.tereschenko.Task4;

public final class Constants {

	private Constants() {
	}

	public static final int K = 5;

	public static final int T = 6;

	public static final int DIVIDER = 5;

	public static final String SEP = ";";

	public static final String EOL = System.lineSeparator();

	public static final String INPUT_RAW = "input_raw.txt";

	public static final String INPUT = "input.txt";

	public static final String LASTNAME = "Lastname_";

	public static final String TEST = "Test_";
}
