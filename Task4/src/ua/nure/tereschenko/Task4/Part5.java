package ua.nure.tereschenko.Task4;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class Part5 {

	public static void process(Data data) {
		Map<String, List<String>> students = new HashMap<>();
		for (int i = 1; i <= Constants.K; i++) {
			List<String> tests = new ArrayList<>();
			for (int j = 0; j < Constants.T; j++) {
				if (data.getStudentsMap().get(Constants.LASTNAME + i).get(j) == null) {
					tests.add(Constants.TEST + String.valueOf((j + 1)));
				}
			}
			Collections.sort(tests, new Comparator<String>() {

				@Override
				public int compare(String o1, String o2) {
					return o2.compareTo(o1);
				}
			});
			students.put(Constants.LASTNAME + i, tests);
		}
		Map<String, List<String>> sortedMap = Utils.sort(students);
		for (Entry<String, List<String>> entry : sortedMap.entrySet()) {
			System.out.println(entry.getKey() + " ==> "
					+ entry.getValue().size() + ": "
					+ entry.getValue().toString());
		}
	}

	public static void main(String[] args) throws FileNotFoundException {
		Data data = new Data(Constants.INPUT);
		process(data);
	}
}
