package ua.nure.tereschenko.Task3.part2;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class Part2 {

	private static final String FILE_NAME = "part2.txt";

	private static final String FILE_NAME2 = "part2_sorted.txt";

	private static final int N = 20;

	private static final int MAX = 50;

	public static void main(String[] args) throws IOException {
		OutputStreamWriter fw = new OutputStreamWriter(new FileOutputStream(
				FILE_NAME2), Charset.forName("Cp1251"));
		int count = 0;
		while (count < N) {
			Random r = new Random();
			int random = r.nextInt(MAX);
			fw.write(String.valueOf(random));
			fw.write(' ');
			count++;
		}
		fw.flush();
		fw.close();
		Scanner scanner = new Scanner(new File(FILE_NAME), "Cp1251");
		List<Integer> content = new ArrayList<Integer>();
		System.out.print("input  ==> ");
		while (scanner.hasNextInt()) {
			int element = scanner.nextInt();
			content.add(element);
			System.out.print(element + " ");
		}
		scanner.close();
		Collections.sort(content);
		System.out.print("\noutput ==> ");
		fw = new OutputStreamWriter(new FileOutputStream(FILE_NAME2),
				Charset.forName("Cp1251"));
		for (Integer item : content) {
			System.out.print(item + " ");
			fw.write(String.valueOf(item));
			fw.write(' ');
		}
		fw.close();
	}
}