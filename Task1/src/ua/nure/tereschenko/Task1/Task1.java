package ua.nure.tereschenko.Task1;

public class Task1 {

	public static void usage() {
		System.out.println("Usage: java "
				+ "ua.kharkov.tereschenko.Task1.Task1 X Y");
	}

	public static int gcd(int first, int second) {
		if (first < 0 || second < 0) {
			throw new IllegalArgumentException(
					"Values of method must be positive");
		}
		if (first == second) {
			return first;
		}
		if (first > second) {
			return gcd(first - second, second);
		}
		return gcd(first, second - first);
	}

	public static void main(String[] args) {
		if (args.length != 2) {
			usage();
			return;
		}
		int x = Integer.parseInt(args[0]);
		int y = Integer.parseInt(args[1]);
		int gcd = gcd(x, y);

		String output = String.format("NOD x = %d, y = %d --> %d", x, y, gcd);
		System.out.println(output);
	}
}
