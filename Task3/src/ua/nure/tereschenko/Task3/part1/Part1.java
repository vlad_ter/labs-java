package ua.nure.tereschenko.Task3.part1;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Part1 {

	private static final String FILE_NAME = "part1.txt";

	private static final String ENCODING = "Cp1251";

	private static final String EOL = System.lineSeparator();

	private static final String REGEXP = "(\\S+)|(\\s+)";

	public static void main(String[] args) throws FileNotFoundException {
		String content = null;
		StringBuilder sb = new StringBuilder();
		Scanner s = new Scanner(new File(FILE_NAME), ENCODING);
		while (s.hasNextLine()) {
			sb.append(s.nextLine()).append(EOL);
		}
		content = sb.substring(0, sb.length() - EOL.length());
		s.close();
		Pattern pattern = Pattern.compile(REGEXP);
		Matcher matcher = pattern.matcher(content);
		StringBuilder result = new StringBuilder();
		String temp = null;
		while (matcher.find()) {
			temp = matcher.group().length() > 2 ? matcher.group().toUpperCase()
					: matcher.group();
			result.append(temp);
		}
		System.out.println(result);
	}

}